/**
 * @file manager.ts
 * @author Alejandro D. Simi
 */
import { ConfigsManager } from '../configs';
import { DRCollector } from '../drcollector';
import { GenericManager, IItemSpec, TAB } from '../includes';
import { ITasksManagerOptions, ITasksManager_TasksResponse, Task, TasksConstants, TasksList } from '.';
import chalk from 'chalk';

export class TasksManager extends GenericManager<ITasksManagerOptions> {
    //
    // Protected properties.
    protected _consumingQueue: boolean = false;
    protected _intervals: NodeJS.Timeout[] = [];
    protected _items: TasksList | null = null;
    protected _paused: string[] = [];
    protected _queue: any[] = [];
    protected _queueInterval: NodeJS.Timeout | null = null;
    protected _stopped: boolean = false;
    protected _shutDown: boolean = false;
    //
    // Constructor.
    constructor(directories: string[] | string, options: ITasksManagerOptions | null = null, configs: ConfigsManager | null = null) {
        super(directories, options, configs);
        this._valid = !this._lastError;

        DRCollector.registerTasksManager(this);
    }
    //
    // Public methods.
    public isShutDown(): boolean {
        return this._shutDown;
    }
    public async load(): Promise<boolean> {
        // istanbul ignore else
        if (!this._loaded) {
            this._loaded = true;

            // istanbul ignore else
            if (this._options?.verbose) {
                console.log(`Loading tasks:`);
            }

            this._items = {};
            // istanbul ignore else
            if (!this._lastError && this._itemSpecs.length > 0) {
                for (let item of this._itemSpecs) {
                    try {
                        // istanbul ignore else
                        if (this._options?.verbose) {
                            console.log(`${TAB}- '${chalk.green(item.name)}'`);
                        }

                        const task: Task = require(item.path);
                        task.setCoreName(item.name);
                        task.setManager(this);
                        task.setConfigs(this._configs);
                        this._items[item.name] = <Task>task;
                    } catch (err) {
                        console.error(chalk.red(`Unable to load task '${item.name}'.`), err);
                    }
                }
            }

            this._valid = !this._lastError;

            await this.runAtStart();
            this.setIntervals();
        }

        return this.valid();
    }
    public pause(key: string): void {
        // istanbul ignore else
        if (!this._shutDown && !this._paused.includes(key)) {
            this._paused.push(key);
        }
    }
    public paused(): string[] {
        return [...this._paused];
    }
    public tasks(): ITasksManager_TasksResponse[] {
        return this._itemSpecs.map((item: IItemSpec): ITasksManager_TasksResponse => {
            const task: Task = (<TasksList>this._items)[item.name];
            return {
                code: task.code(),
                description: task.description() || '',
                interval: task.getInterval(),
                name: task.name(),
                path: item.path,
            };
        });
    }
    public shutDown(): void {
        // istanbul ignore else
        if (!this.isShutDown()) {
            this._shutDown = true;

            this.stop();
            if (this._options?.runAsQueue) {
                this._consumingQueue = true;
                this._queueInterval && clearInterval(this._queueInterval);
                this._queueInterval = null;
            }

            for (const interval of this._intervals) {
                clearInterval(interval);
            }
        }
    }
    public stop(): void {
        this._stopped = true;
    }
    public stopped(): boolean {
        return this._stopped;
    }
    public unpause(key: string): void {
        // istanbul ignore else
        if (!this.isShutDown()) {
            this._paused = this._paused
                .filter((k: string): boolean => k !== key);
        }
    }
    public unstop(): void {
        // istanbul ignore else
        if (!this.isShutDown()) {
            this._stopped = false;
        }
    }
    //
    // Protected methods.
    /* istanbul ignore next */
    protected async consumeQueue(): Promise<void> {
        if (this._options?.debug) {
            console.log(chalk.yellow(`DRTools::TasksManager: Consuming task from queue (queue count: ${this._queue.length})...`));
        }

        if (!this._consumingQueue && this._queue.length > 0) {
            this._consumingQueue = true;

            if (this._options?.debug) {
                console.log(chalk.yellow(`DRTools::TasksManager: Running task...`));
            }

            const task: Function = this._queue.shift();
            await task();

            if (this._options?.debug) {
                console.log(chalk.yellow(`DRTools::TasksManager: Task done (queue count: ${this._queue.length}).`));
            }

            this._consumingQueue = false;
        } else if (this._consumingQueue && this._options?.debug) {
            console.log(chalk.yellow(`DRTools::TasksManager: A task is already running (queue count: ${this._queue.length}).`));
        }
    }
    /* istanbul ignore next */
    protected cleanOptions(): void {
        this._options = {
            debug: false,
            queueTick: 5000,
            runAsQueue: false,
            suffix: TasksConstants.Suffix,
            verbose: true,

            ...this._options !== null ? this._options : {},
        };
    }
    /* istanbul ignore next */
    protected async runAtStart(): Promise<void> {
        if (this.valid()) {
            for (const [, task] of Object.entries(<TasksList>this._items)) {
                if (task.shouldRunAtStart()) {
                    await task.coreRun();
                }
            }
        }
    }
    /* istanbul ignore next */
    protected setIntervals(): void {
        if (this.valid()) {
            for (const [key, task] of Object.entries(<TasksList>this._items)) {
                //
                // Are tasks being run when their time comes up?, or when their
                // time comes up are they being queued for the next queue
                // available tick?
                if (this._options?.runAsQueue) {
                    this._intervals.push(setInterval(() => {
                        //
                        // Checking if tasks are stopped of the requested task is individually
                        // stopped.
                        if (!this.stopped() && !this._paused.includes(key)) {
                            this._queue.push(() => task.coreRun());

                            if (this._options?.debug) {
                                console.log(chalk.yellow(`DRTools::TasksManager: Task '${task.name()}' pushed into queue (queue count: ${this._queue.length}).`));
                            }
                        }
                    }, task.getInterval()));
                } else {
                    this._intervals.push(setInterval(() => {
                        //
                        // Checking if tasks are stopped of the requested task is individually
                        // stopped.
                        if (!this.stopped() && !this._paused.includes(key)) {
                            if (this._options?.debug) {
                                console.log(chalk.yellow(`DRTools::TasksManager: Running task '${task.name()}'.`));
                            }

                            task.coreRun();
                        }
                    }, task.getInterval()));
                }
            }
            //
            // Setting a main interval to consume queued tasks.
            if (this._options?.runAsQueue) {
                this._queueInterval = setInterval(() => this.consumeQueue(), this._options?.queueTick || 5000);
            }
        }
    }
}
