/**
 * @file task.ts
 * @author Alejandro D. Simi
 */
import { ConfigsManager } from '../configs';
import { TasksManager } from '.';
import chalk from 'chalk';

export abstract class Task {
    //
    // Protected properties.
    protected _configs: ConfigsManager | null = null;
    protected _coreName: string = '';
    protected _manager: TasksManager | null = null;
    protected _overloads: number = 0;
    protected canOverload: boolean = false;
    protected interval: number = 5000;
    protected runAtStart: boolean = false;
    //
    // Constructor.
    public constructor() {
        this.load();
    }
    //
    // Public methods.
    public code(): string {
        return this._coreName;
    }
    public async coreRun(): Promise<void> {
        // istanbul ignore else
        if (await this.coreShouldRun()) {
            this._overloads++;

            try {
                await this.run();
            } catch (err: any) {
                console.error(chalk.yellow(`DRTools::Tasks: There was an error running task '${this.name()}'.`), err);
            }

            this._overloads--;
        }
    }
    public description(): string | null {
        return null;
    }
    public getInterval(): number {
        return this.interval;
    }
    public abstract name(): string;
    public paused(): boolean {
        return this._manager !== null
            && !this._manager.stopped()
            && !this._manager.paused().includes(this.code());
    }
    public abstract run(): Promise<void>;
    public running(): boolean {
        return this._overloads > 0;
    }
    public setConfigs(configs: ConfigsManager | null): void {
        this._configs = configs;
    }
    public setCoreName(coreName: string): void {
        this._coreName = coreName;
    }
    public setManager(manager: TasksManager): void {
        this._manager = manager;
    }
    public shouldRunAtStart(): boolean {
        return this.runAtStart;
    }
    //
    // Protected methods.
    /* istanbul ignore next */
    protected async coreShouldRun(): Promise<boolean> {
        return (!this.running() || this.canOverload) && await this.shouldRun();
    }
    /* istanbul ignore next */
    protected load(): void {
    }
    /* istanbul ignore next */
    protected async shouldRun(): Promise<boolean> {
        return true;
    }
}