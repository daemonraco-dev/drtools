/**
 * @file tenedor-child.ts
 * @author Alejandro D. Simi
 */

import { ChildProcess } from 'child_process';
import { ITenedorChildResponse } from './types';
import { TenedorNoChildException } from './exceptions';

export class TenedorChild {
    //
    // Properties.
    protected childProcess: ChildProcess | null = null;
    //
    // Construction.
    constructor(child: ChildProcess) {
        this.childProcess = child;
    }
    //
    // Public methods.
    public async ask<D = any, R = any>(data: D): Promise<R | null> {
        this.checkRunning();
        //
        // Creating listeners for the process. It assumes that the first message
        // sent by the child is the one and only that's going to be sent before
        // exiting.
        const childResponse: Promise<R> = new Promise<R>((resolve: (r: R) => void, reject: (e: any) => void): void => {
            this.childProcess?.once('message', (response: ITenedorChildResponse<R>): void =>
                response.error ? reject(response.error) : resolve(response.data));
        });
        const childExit: Promise<R> = new Promise<R>((resolve: (r: R) => void, reject: (e: any) => void): void => {
            this.childProcess?.once('exit', (code: number): void =>
                reject(`The process exited unexpectedly with code ${code}.`));
        });
        //
        // Sending a question to the child and listening for it's response.
        await new Promise<void>((resolve: () => void, reject: (e: any) => void): void => {
            this.childProcess?.send(<any>data, (error: Error | null) =>
                error ? reject(error) : resolve());
        });

        return Promise.race([childResponse, childExit]);
    }
    public child(): ChildProcess | null {
        return this.childProcess;
    }
    public kill(signal?: number | NodeJS.Signals) {
        //
        // Whether the child finished correctly or not, it has to be stopped.
        // istanbul ignore else
        if (this.childProcess && this.childProcess.exitCode === null) {
            this.childProcess.kill(signal);
            this.childProcess = null;
        }
    }
    public async tell<D = any>(data: D): Promise<void> {
        this.checkRunning();
        //
        // Sending a question to the child and listening for it's response.
        await new Promise<void>((resolve: () => void, reject: (e: any) => void): void => {
            this.childProcess?.send(<any>data, (error: Error | null) =>
                error ? reject(error) : resolve());
        });
    }
    //
    // Protected methods.
    protected checkRunning(): void {
        //
        // Does it have a valid child process?
        // istanbul ignore else
        if (!this.childProcess) {
            throw new TenedorNoChildException(`Child process is not running.`);
        }
    }
}