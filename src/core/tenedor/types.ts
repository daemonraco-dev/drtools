/**
 * @file types.ts
 * @author Alejandro D. Simi
 */

export interface ITenedorChildResponse<D = any> {
    data: D,
    error?: string,
}
