/**
 * @file tenedor.ts
 * @author Alejandro D. Simi
 */

import { ChildProcess, fork } from 'child_process';
import { glob } from 'glob';
import { TenedorChild } from './tenedor-child';
import { TenedorException, TenedorMissingScriptException } from './exceptions';
import fs from 'fs-extra';

export class Tenedor {
    //
    // Construction.
    // istanbul ignore next
    private constructor() {
        throw new TenedorException(`This is a factory-class, it's not possible to create instances of it.`);
    }
    //
    // Public methods.
    public static async Run<D = any, R = any>(script: string, data: D): Promise<R | null> {
        let results: R | null = null;

        const tenedorChild: TenedorChild = await Tenedor.Spawn(script);
        //
        // Telling the child to start and listening for it's response.
        try {
            results = await tenedorChild.ask(data);
        } finally {
            tenedorChild.kill();
        }

        return results;
    }
    public static async Spawn(script: string): Promise<TenedorChild> {
        //
        // Does the script exists?
        let scriptPaths: string[] = [];
        if (fs.existsSync(script)) {
            scriptPaths = [script];
        } else {
            scriptPaths = glob.sync(`${script}.?s`);
        }
        // istanbul ignore else
        if (scriptPaths.length !== 1) {
            throw new TenedorMissingScriptException(script, `'${script}' doesn't exist.`);
        }
        //
        // Spawning a new child.
        const child: ChildProcess = fork(scriptPaths[0]);
        //
        // Checking that it actually launches.
        const childSpawn: Promise<boolean> = new Promise<boolean>((resolve: (r: boolean) => void, reject: (e: any) => void): void => {
            child.once('spawn', (): void => resolve(true));
        });
        const childError: Promise<boolean> = new Promise<boolean>((resolve: (r: boolean) => void, reject: (e: any) => void): void => {
            child.once('error', (err: Error): void => reject(err));
        });
        await Promise.race([childSpawn, childError]);

        return new TenedorChild(child);
    }
}
