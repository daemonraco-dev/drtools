/**
 * @file exceptions.ts
 * @author Alejandro D. Simi
 */

export class TenedorException extends Error {
}

export class TenedorMissingScriptException extends Error {
    //
    // Properties.
    public readonly script: string = '';
    //
    // Construction.
    constructor(script: string, message: string) {
        super(message);
        this.script = script;
    }
}

export class TenedorNoChildException extends Error {
}
