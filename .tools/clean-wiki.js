/* istanbul ignore file */
'use strict';

const fetch = require('node-fetch');

(async () => {
    try {
        let wikis = [];
        let branches = [];

        const branchesResponse = await fetch(`${process.env.CI_API_V4_URL}/projects/${process.env.CI_PROJECT_ID}/repository/branches`, {
            method: 'get',
            headers: { 'PRIVATE-TOKEN': process.env.DRTOOLS_API_TOKEN },
        });
        if (branchesResponse.ok) {
            branches = (await branchesResponse.json())
                .map(b => b.name.replace(/[/]/g, '_'));
        }

        const wikisResponse = await fetch(`${process.env.CI_API_V4_URL}/projects/${process.env.CI_PROJECT_ID}/wikis?content=0`, {
            method: 'get',
            headers: { 'PRIVATE-TOKEN': process.env.DRTOOLS_API_TOKEN },
        });
        if (wikisResponse.ok) {
            wikis = (await wikisResponse.json())
                .map(b => b.slug);
        }

        const slugsToRemove = [];
        const slugPattern = /^branches\/(.+)\/report$/;
        for (const slug of wikis) {
            const match = slug.match(slugPattern);
            if (match && !branches.includes(match[1])) {
                slugsToRemove.push(slug);
            }
        }

        for (const slug of slugsToRemove) {
            console.log(`Deleting wiki 'https://gitlab.com/daemonraco-dev/drtools/-/wikis/${slug}'...`);
            const response = await fetch(`${process.env.CI_API_V4_URL}/projects/${process.env.CI_PROJECT_ID}/wikis/${encodeURIComponent(slug)}`, {
                method: 'delete',
                keepalive: false,
                headers: { 'PRIVATE-TOKEN': process.env.DRTOOLS_API_TOKEN },
            });
            if (!response.ok) {
                throw new Error(`Unable to delete wiki (HTTP-${response.status}: ${response.statusText})`);
            }
        }

        process.exit();
    } catch (err) {
        console.error(`${err}`);
        process.exit(-1);
    }
})();
