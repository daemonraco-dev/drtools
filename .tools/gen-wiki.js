/* istanbul ignore file */
'use strict';

const fetch = require('node-fetch');
const FormData = require('form-data');
const fs = require('fs-extra');
const path = require('path');

const ciVar = name => process.env[name] || 'local';

const generate = async dir => {
    let md = `# ${(new Date()).toISOString()}\n`;
    md += `## Information\n`;
    //
    // Main information.
    md += `\n`;
    md += `* __Created by__: [${ciVar('CI_JOB_STAGE')}::${ciVar('CI_JOB_NAME')} (${ciVar('CI_JOB_ID')})](${ciVar('CI_JOB_URL')})\n`;
    md += `* __Pipeline__: [#${ciVar('CI_PIPELINE_ID')}](${ciVar('CI_PIPELINE_URL')}) (Source: ${ciVar('CI_PIPELINE_SOURCE')})\n`;
    md += `\n`;
    //
    // Versions.
    md += `## Versions\n\n`;
    md += `| Software  | Version |\n`;
    md += `|:----------|:--------|\n`;

    const mainPackage = require(path.join(__dirname, '../package.json'));
    const mainVersion = mainPackage.version.replace(/[\^~]/g, '');
    md += `| Version   | \`v${mainVersion}\` |\n`;
    const angularPackage = require(path.join(__dirname, '../web-ui/ui-ng/package.json'));
    const angularVersion = angularPackage.dependencies['@angular/core'].replace(/[\^~]/g, '');
    const bootstrapVersion = angularPackage.dependencies['bootstrap'].replace(/[\^~]/g, '');
    md += `| Angular   | \`v${angularVersion}\` |\n`;
    md += `| Bootstrap | \`v${bootstrapVersion}\` |\n`;
    md += `\n`;
    //
    // Coverage.
    const coveragePackage = require(path.join(__dirname, '../coverage/coverage-summary.json'));
    md += `## Coverage\n\n`;
    md += `| Label      | Value  |\n`;
    md += `|:-----------|:-------|\n`;
    md += `| Statements | ${coveragePackage.total.statements.pct}% |\n`;
    md += `| Branches   | ${coveragePackage.total.branches.pct}% |\n`;
    md += `| Functions  | ${coveragePackage.total.functions.pct}% |\n`;
    md += `| Lines      | ${coveragePackage.total.lines.pct}% |\n`;
    //
    // Prompting for pipeline logs.
    console.log(md);
    //
    // Storing.
    await fs.writeFile(path.join(dir, 'readme.md'), md);
};

const upload = async dir => {
    //
    // Creating code.
    let md = (await fs.readFile(path.join(dir, 'readme.md'))).toString();

    const wikiSlug = `branches/${process.env.CLEAN_BRANCH}/report`;

    const wikis = await fetch(`${process.env.CI_API_V4_URL}/projects/${process.env.CI_PROJECT_ID}/wikis/${encodeURIComponent(wikiSlug)}`, {
        method: 'get',
        headers: { 'PRIVATE-TOKEN': process.env.DRTOOLS_API_TOKEN },
    });
    if (wikis.status === 200) {
        md = `${(await wikis.json()).content}\n${md}`;
        console.log(`Deleting wiki 'https://gitlab.com/daemonraco-dev/drtools/-/wikis/${wikiSlug}'...`);
        const response = await fetch(`${process.env.CI_API_V4_URL}/projects/${process.env.CI_PROJECT_ID}/wikis/${encodeURIComponent(wikiSlug)}`, {
            method: 'delete',
            keepalive: false,
            headers: { 'PRIVATE-TOKEN': process.env.DRTOOLS_API_TOKEN },
        });
        if (!response.ok) {
            throw new Error(`Unable to delete wiki (HTTP-${response.status}: ${response.statusText})`);
        }
    } else if (wikis.status !== 404) {
        throw new Error(`Unable to get wikis (HTTP-${wikis.status}: ${wikis.statusText})`);
    }

    console.log(`Creating wiki 'https://gitlab.com/daemonraco-dev/drtools/-/wikis/${wikiSlug}'...`);
    const wikiBody = new FormData();
    wikiBody.append('format', 'markdown');
    wikiBody.append('content', md);
    wikiBody.append('title', wikiSlug);
    const response = await fetch(`${process.env.CI_API_V4_URL}/projects/${process.env.CI_PROJECT_ID}/wikis`, {
        method: 'post',
        keepalive: false,
        headers: { 'PRIVATE-TOKEN': process.env.DRTOOLS_API_TOKEN },
        body: wikiBody,
    });
    if (!response.ok) {
        throw new Error(`Unable to create wiki (HTTP-${response.status}: ${response.statusText})`);
    }
};

(async () => {
    try {
        const reportsDirectory = path.join(__dirname, '../wikis');
        if (!fs.existsSync(reportsDirectory)) {
            await fs.mkdir(reportsDirectory);
        }

        switch (process.argv[2]) {
            case 'generate':
                await generate(reportsDirectory);
                break;
            case 'upload':
                await upload(reportsDirectory);
                break;
            default:
                console.error(`Unknown option '${process.argv[2]}'.`);
                break;
        }

        process.exit();
    } catch (err) {
        console.error(`${err}`);
        process.exit(-1);
    }
})();
