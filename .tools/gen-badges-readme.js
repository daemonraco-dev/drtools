/* istanbul ignore file */
'use strict';

const { glob } = require('glob');
const fs = require('fs-extra');
const path = require('path');

(async () => {
    try {
        //
        // Loading badges.
        const badges = glob.sync(path.join(__dirname, '../badges/*.svg'))
            .map(b => path.basename(b));

        const badgesMD = path.join(__dirname, '../badges/README.md');
        //
        // Creating code.
        let md = `# Badges\n\n`;
        md += `| Code | Badge |\n`;
        md += `|:-----|:------|\n`;
        for (const badge of badges) {
            md += `| \`${badge}\` | ![${badge}](${badge}) |\n`;
        }
        //
        // Storing into a readme.
        await fs.writeFile(badgesMD, md);

        process.exit();
    } catch (err) {
        console.error(`${err}`);
        process.exit(-1);
    }
})();
