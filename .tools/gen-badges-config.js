/* istanbul ignore file */
'use strict';

const fs = require('fs-extra');
const path = require('path');

(async () => {
    const configPath = path.join(__dirname, '../badges/config.json');
    const config = { badges: [] };

    const angularPackage = require(path.join(__dirname, '../web-ui/ui-ng/package.json'));

    const angularVersion = angularPackage.dependencies['@angular/core'].replace(/[\^~]/g, '');
    config.badges.push({
        'angular-version': {
            label: 'Angular Version',
            value: angularVersion,
            color: '#b52e31',
        },
    });
    const bootstrapVersion = angularPackage.dependencies['bootstrap'].replace(/[\^~]/g, '');
    config.badges.push({
        'bootstrap-version': {
            label: 'Bootstrap',
            value: bootstrapVersion,
            color: '#7552cc',
        },
    });

    await fs.writeFile(configPath, JSON.stringify(config, null, 4));

    process.exit();
})();
