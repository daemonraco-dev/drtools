import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BooleanComponent } from './boolean/boolean.component';
import { ExpandCollapseComponent } from './expand-collapse/expand-collapse.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
    declarations: [
        BooleanComponent,
        ExpandCollapseComponent,
        FooterComponent,
        NavbarComponent,
    ],
    exports: [
        BooleanComponent,
        ExpandCollapseComponent,
        FooterComponent,
        NavbarComponent,
    ],
    imports: [
        CommonModule,
        FontAwesomeModule,
        RouterModule,
    ],
})
export class BasicsModule { }
