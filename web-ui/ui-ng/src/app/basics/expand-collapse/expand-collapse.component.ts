import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-expand-collapse',
    templateUrl: './expand-collapse.component.html',
})
export class ExpandCollapseComponent implements OnInit {
    //
    // Properties.
    @Output() public expandAll: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();
    @Output() public hideAll: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();
    //
    // Construction.
    constructor() {
    }
    //
    // Public methods.
    public ngOnInit(): void {
    }
    public triggerExpandAll(event: MouseEvent): void {
        this.expandAll.emit(event);
    }
    public triggerHideAll(event: MouseEvent): void {
        this.hideAll.emit(event);
    }
    //
    // Protected methods.
}
