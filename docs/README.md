<!-- version-check:0.17.0 -->
<!-- version-warning -->
<!-- /version-warning -->

# DAEMonRaco's Tools (drtools)
... no, it's not _Doctor Tools_ :)

## What is it?
This is a simple set of useful tools that can accelerate the development of a node
application.
WOW! that sounds awesome... well, not really.

## Should I use it?
_Short answer?_ No.

_Long answer?_ Well, if you want, you can give it a try, but it's up to you.

## Provided tools
- Main Tools
    - [Home](README.md)
    - [Configs](configs.md)
    - [Tasks](tasks.md)
    - [Routes](routes.md)
    - [Middlewares](middlewares.md)
    - [Mock-up Routes](mock-routes.md)
    - [Mock-up Endpoints](endpoints.md)
    - [Loaders](loaders.md)
    - [Plugins](plugins.md)
    - [Connector](connector.md)
    - Tools
        - [Tenedor](tenedor.md)
- Commands
    - [Server](server.md)
    - [Generator](generator.md)

## Breaking changes
* If you're migrating from a version `1.15.9` or previous to `1.16.0`, you should
read the documentation of [tasks](tasks.md) in order to accommodate your code to
the latest changes.

## License
[MIT](https://opensource.org/licenses/MIT) &copy;2018-2022
[Alejandro Dario Simi](http://daemonraco.com)
