<!-- version-check:0.17.0 -->
<!-- version-warning -->
<!-- /version-warning -->

# Tenedor

## What is this tool?
This is an easy to use set of tools that let you work with forking child process.

## Run()
Lets's say you have a pieces of code that runs based on some set of values and
returns some other set of values, just like a function, but you what to run this
in parallel using `fork` from `child_process`.
In this cases you can use `Tenedor.Run()`.

For your piece of code let's say it's encapsulated like this in a file called
`my-parallel-code.ts` (this example is written in _TypeScript_ but you can use
simple _Javascript_):

```typescript
import { IInput, IOutput } from './my-types';
import { ITenedorChildResponse } from 'drtools';

process.on('message', async ({ value }: IInput): Promise<void> => {
    const response: ITenedorChildResponse<IOutput> = {
        data: {
            result1: null,
            result2: null,
        },
    };

    try {
        response.data.result1 = await someHeavyFunction(value);
        response.data.result2 = await anotherHeavyFunction(value);
    } catch(err: any) {
        response.error = `${err}`;
    }

    process.send && process.send(response);
});
```
At this point you can call your script as a child process with something like
this:

```typescript
const result: IOutput | null = await Tenedor.Run('/path/to/my-parallel-code', {
    value: 200,
});
console.log(result);
```

Doing this will:

* spawn a child process based on your script,
* send the second parameter as a message which will trigger your script to
eventually run `someHeavyFunction()` and `anotherHeavyFunction()`,
* receive results (in a message),
* stop the child process and
* return the generated data.

## Spawn()
A more complex way to use `Tenedor` that doesn't restricts you to one run per
spawned process, is to use `Tenedor.Spawn()`.
This method takes your script path (as a string) and returns an object of class
`TenedorChild` which is a decorator to `ChildProcess` from _NodeJS_ API, but with
fewer and simpler methods.

### Creating a child process
To create a child process you can write something like this:

```typescript
const child: TenedorChild = await Tenedor.Spawn('/path/to/my-parallel-code');
```

### Stopping a child process
If you created a `TenedorChild` and want to stop it, run this:

```typescript
child.kill();
```

This will hand over a _kill_ signal so you can pass your preferred signal code.

### Sending information
If you need to send information to the child process, but you don't need for it to
generate a response message, you can do this:

```typescript
await child.tell({
    value: 200,
});
```

>This is asynchronous because it waits for the message to be actually delivered.

### Asking for information
If you want the child process to give you some information based on some payload,
you can do something like this:

```typescript
const result: IOutput | null = await child.ask({
    value: 200,
});
console.log(result);
```

>This will block your code until the child process sends a message back.
