'use strict';
//
// Dependencies.
const { assert } = require('chai');
const path = require('path');
//
// Testing.
describe(`[011] drtools: Tools:`, () => {
    const { Tools, ToolsCheckPath } = require('..');

    it(`checks that it can't create an instance`, async () => {
        try {
            new Tools();
            assert.isTrue(false, 'it should not create an instance.');
        } catch (err) {
            assert.match(`${err}`, /^.*factory-class.*not possible.*create instances.*$/);
        }
    });

    it(`merges two arrays`, async () => {
        const left = [1, 3, 5];
        const right = [2, 4, 6];
        assert.sameMembers(Tools.DeepMergeObjects(left, right), [1, 2, 3, 4, 5, 6]);
    });

    it(`merges two simple objects`, async () => {
        const left = {
            a: 10,
            b: 'b',
        };
        const right = {
            b: 'b2',
            c: true,
        };
        assert.deepNestedInclude(Tools.DeepMergeObjects(left, right), {
            a: 10,
            b: 'b2',
            c: true,
        });
    });

    it(`deep merges two objects`, async () => {
        const left = {
            a: 10,
            b: 'b',
            sub: {
                a: 20,
                b: 'bb',
            },
        };
        const right = {
            b: 'b2',
            c: true,
            sub: {
                b: 'bb22',
                c: false,
            }
        };
        assert.deepNestedInclude(Tools.DeepMergeObjects(left, right), {
            a: 10,
            b: 'b2',
            c: true,
            sub: {
                a: 20,
                b: 'bb22',
                c: false,
            },
        });
    });

    it(`launches a simple retry block`, async () => {
        await Tools.BlockRetry(async ({ value }) => {
            assert.isUndefined(value);
        });
    });

    it(`launches a simple retry block`, async () => {
        await Tools.BlockRetry(async ({ value }) => {
            assert.isUndefined(value);
        });
    });

    it(`launches a simple retry block with parameters`, async () => {
        await Tools.BlockRetry(async ({ value }) => {
            assert.strictEqual(value, 10);
        }, { params: { value: 10 } });
    });

    it(`launches a simple retry block that fails`, async function () {
        this.timeout(5000);

        let tries = 0;
        try {
            await Tools.BlockRetry(async ({ value }) => {
                tries++;
                throw 'failed';
            }, { params: { value: 10 } });
        } catch (err) {
            assert.strictEqual(`${err}`, 'failed');
        }

        assert.strictEqual(tries, 3);
    });

    it(`launches a simple retry block that fails (2 retries)`, async function () {
        this.timeout(5000);

        let tries = 0;
        try {
            await Tools.BlockRetry(async ({ value }) => {
                tries++;
                throw 'failed';
            }, {
                maxRetries: 2,
                params: { value: 10 },
            });
        } catch (err) {
            assert.strictEqual(`${err}`, 'failed');
        }

        assert.strictEqual(tries, 2);
    });

    it(`checks a file path`, () => {
        let result = null;

        result = Tools.CheckFile('./tmp/test-server.log');
        assert.strictEqual(result.status, ToolsCheckPath.DoesntExist);
        assert.strictEqual(result.originalPath, './tmp/test-server.log');
        assert.strictEqual(result.path, './tmp/test-server.log');
        assert.isNull(result.stat);

        result = Tools.CheckFile(path.join(__dirname, 'tmp/test-server.log'));
        assert.strictEqual(result.status, ToolsCheckPath.Ok);
        assert.strictEqual(result.originalPath, path.join(__dirname, 'tmp/test-server.log'));
        assert.strictEqual(result.path, path.join(__dirname, 'tmp/test-server.log'));
        assert.isNotNull(result.stat);
        assert.typeOf(result.stat, 'object');

        result = Tools.CheckFile('./tmp/test-server.log', __dirname);
        assert.strictEqual(result.status, ToolsCheckPath.Ok);
        assert.strictEqual(result.originalPath, './tmp/test-server.log');
        assert.strictEqual(result.path, path.join(__dirname, './tmp/test-server.log'));
        assert.isNotNull(result.stat);
        assert.typeOf(result.stat, 'object');
    });

    it(`gets a random string`, () => {
        const result = Tools.RandomKey();
        assert.match(result, /^[0-9a-z]+$/)
    });

    it(`gets current version`, () => {
        const { version } = require('../package.json');
        assert.strictEqual(Tools.Version(), version);
    });

    it(`runs environment checks`, () => {
        assert.isTrue(Tools.IsKoa({ context: {} }));
        assert.isFalse(Tools.IsExpress({ context: {} }));
        assert.isFalse(Tools.IsKoa({}));
        assert.isTrue(Tools.IsExpress({}));
    });
});
