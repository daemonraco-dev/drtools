'use strict';
//
// Dependencies.
const { assert } = require('chai');
//
// Testing.
describe(`[009] drtools: Hooks:`, () => {
    const { Hook, HookEvents } = require('..');

    it(`makes it handle events`, async () => {
        const hook = new Hook('test-hook');

        assert.strictEqual(hook.key(), 'test-hook');

        hook.addListener('tester-1', async (bait) => {
            assert.deepNestedInclude(bait, {
                message: 'this is a message',
            });
            return {
                ...bait,
                code: 10,
            };
        });

        hook.addListener('tester-2', async (bait) => {
            assert.deepNestedInclude(bait, {
                message: 'this is a message',
            });
            return bait;
        });

        const results = await hook.reelIn({
            message: 'this is a message',
        });

        assert.deepNestedInclude(results, {
            'tester-1': {
                message: 'this is a message',
                code: 10,
            },
            'tester-2': {
                message: 'this is a message',
            }
        });

        assert.sameMembers(hook.listenerCodes(), ['tester-1', 'tester-2']);
        hook.removeListener('tester-1');
        assert.sameMembers(hook.listenerCodes(), ['tester-2']);
        hook.removeListener('tester-2');
        assert.sameMembers(hook.listenerCodes(), []);
        hook.removeListener('tester-3');
    });

    it(`makes it handle events with cache active`, async () => {
        const hook = new Hook('test-hook');

        assert.strictEqual(hook.key(), 'test-hook');

        assert.isFalse(hook.isCached());
        hook.activateCache();
        assert.isTrue(hook.isCached());

        hook.addListener('tester-1', async (bait) => {
            assert.deepNestedInclude(bait, {
                message: 'this is a message',
            });
            return {
                ...bait,
                code: 10,
            };
        });

        hook.addListener('tester-2', async (bait) => {
            assert.deepNestedInclude(bait, {
                message: 'this is a message',
            });
            return bait;
        });

        let results = await hook.reelIn({
            message: 'this is a message',
        });

        assert.deepNestedInclude(results, {
            'tester-1': {
                message: 'this is a message',
                code: 10,
            },
            'tester-2': {
                message: 'this is a message',
            }
        });

        results = await hook.reelIn({
            message: 'this is a message again',
            code: 12,
        });

        assert.deepNestedInclude(results, {
            'tester-1': {
                message: 'this is a message',
                code: 10,
            },
            'tester-2': {
                message: 'this is a message',
            }
        });
    });

    it(`makes it handle events chained`, async () => {
        const hook = new Hook('test-hook');

        assert.strictEqual(hook.key(), 'test-hook');

        hook.addListener('tester-1', async (bait) => {
            assert.deepNestedInclude(bait, {
                message: 'this is a message',
            });
            return {
                ...bait,
                code: 10,
            };
        });

        hook.addListener('tester-2', async (bait) => {
            assert.deepNestedInclude(bait, {
                message: 'this is a message',
                code: 10,
            });
            return {
                ...bait,
                value: true,
            };
        });

        const results = await hook.chainedReelIn({
            message: 'this is a message',
        });

        assert.deepNestedInclude(results, {
            message: 'this is a message',
            code: 10,
            value: true,
        });
    });

    it(`makes it handle events chained with cache active`, async () => {
        const hook = new Hook('test-hook');

        assert.strictEqual(hook.key(), 'test-hook');

        assert.isFalse(hook.isCached());
        hook.activateCache();
        assert.isTrue(hook.isCached());

        hook.addListener('tester-1', async (bait) => {
            assert.deepNestedInclude(bait, {
                message: 'this is a message',
            });
            return {
                ...bait,
                code: 10,
            };
        });

        hook.addListener('tester-2', async (bait) => {
            assert.deepNestedInclude(bait, {
                message: 'this is a message',
                code: 10,
            });
            return {
                ...bait,
                value: true,
            };
        });

        let results = await hook.chainedReelIn({
            message: 'this is a message',
        });

        assert.deepNestedInclude(results, {
            message: 'this is a message',
            code: 10,
            value: true,
        });

        results = await hook.chainedReelIn({
            message: 'this is a message again',
            code: 12,
            value: false,
        });

        assert.deepNestedInclude(results, {
            message: 'this is a message',
            code: 10,
            value: true,
        });
    });

    it(`informs of internal events`, async () => {
        const hook = new Hook('test-hook');

        assert.strictEqual(hook.key(), 'test-hook');

        let hookedCount = 0;
        let unhookedCount = 0;
        hook.on(HookEvents.Hooked, ({ key }) => {
            assert.strictEqual(key, 'tester');
            hookedCount++;
        });
        hook.on(HookEvents.Unhooked, ({ key }) => {
            assert.strictEqual(key, 'tester');
            unhookedCount++;
        });

        assert.strictEqual(hookedCount, 0);
        assert.strictEqual(unhookedCount, 0);

        hook.addListener('tester', async (bait) => bait);
        assert.strictEqual(hookedCount, 1);
        assert.strictEqual(unhookedCount, 0);

        hook.removeListener('tester');
        assert.strictEqual(hookedCount, 1);
        assert.strictEqual(unhookedCount, 1);
    });
});
