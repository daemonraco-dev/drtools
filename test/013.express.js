'use strict';
//
// Dependencies.
const { assert } = require('chai');
//
// Testing.
describe(`[013] drtools: express:`, () => {
    const { ExpressResponseBuilder } = require('../dist/core/express');

    it(`checks that it can't create an instance of ExpressResponseBuilder`, async () => {
        try {
            new ExpressResponseBuilder();
            assert.isTrue(false, 'it should not create an instance.');
        } catch (err) {
            assert.match(`${err}`, /no instances can be created/);
        }
    });
});
