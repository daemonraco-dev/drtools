'use strict';
//
// Dependencies.
const { assert } = require('chai');
//
// Testing.
describe(`[014] drtools: express:`, () => {
    const { KoaResponseBuilder } = require('../dist/core/koa');

    it(`checks that it can't create an instance of KoaResponseBuilder`, async () => {
        try {
            new KoaResponseBuilder();
            assert.isTrue(false, 'it should not create an instance.');
        } catch (err) {
            assert.match(`${err}`, /no instances can be created/);
        }
    });
});
