'use strict';

const chalk = require('chalk');

const { Task } = require('../../..');

class ByTheSecondTask extends Task {
    //
    // Public methods.
    description() {
        return 'A simple task that runs every second';
    }
    name() {
        return 'By the Second';
    }
    async run() {
        console.log(chalk.magenta(`Running 'ByTheSecondTask': ${new Date()}`));
    }
    //
    // Protected methods.
    load() {
        this.interval = 1000;
        this.runAtStart = true;
    }
}

module.exports = new ByTheSecondTask();
