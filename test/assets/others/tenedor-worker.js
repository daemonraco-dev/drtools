const test = value => {
    if (value === false) {
        throw new Error('failed');
    } else if (value === null) {
        process.exit(-1);
    }

    return 10;
}

process.on('message', async ({ value }) => {
    const response = { data: 0 };

    try {
        response.data = await test(value);
    } catch (err) {
        response.error = `${err}`;
    }

    process.send && process.send(response);
});
