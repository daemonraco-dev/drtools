'use strict';
//
// Dependencies.
const { assert } = require('chai');
const path = require('path');
//
// Testing.
describe(`[010] drtools: tasks:`, () => {
    const { Task, TasksManager } = require('..');
    const byTheSecond = require('./tmp/tasks/by-the-second.task');

    class TestClass extends Task {
        name() {
            return 'test';
        }
        async run() {
        }
    }

    it(`tries to load tasks from a directory`, async function () {
        this.timeout(5000);

        let taskRuns = 0;
        const logBackup = console.log;
        console.log = function () {
            taskRuns += (arguments[0] || '').match(/Running 'ByTheSecondTask':/) ? 1 : 0;
            logBackup.apply(this, Array.prototype.slice.call(arguments))
        };

        const dir = path.join(__dirname, 'tmp/tasks');
        const manager = new TasksManager(dir);
        await manager.load();
        await new Promise(r => setTimeout(r, 3000));

        manager.shutDown();
        assert(manager.stopped());
        assert.strictEqual(taskRuns, 3);
        assert.isTrue(manager.isShutDown());

        const tasks = manager.tasks();
        assert.isArray(tasks);
        assert.strictEqual(tasks.length, 1);
        assert.deepNestedInclude(tasks[0], {
            code: 'by-the-second',
            description: 'A simple task that runs every second',
            interval: 1000,
            name: 'By the Second',
            path: path.join(__dirname, 'tmp/tasks/by-the-second.task.js'),
        });

        console.log = logBackup;
    });

    it(`tries to load tasks from a directory in queued mode`, async function () {
        this.timeout(5000);

        let taskRuns = 0;
        const logBackup = console.log;
        console.log = function () {
            taskRuns += (arguments[0] || '').match(/Running 'ByTheSecondTask':/) ? 1 : 0;
            logBackup.apply(this, Array.prototype.slice.call(arguments))
        };

        const dir = path.join(__dirname, 'tmp/tasks');
        const manager = new TasksManager(dir, {
            key: 'test-manager',
            runAsQueue: true,
            queueTick: 500,
        });
        await manager.load();
        await new Promise(r => setTimeout(r, 3000));

        assert.strictEqual(manager.key(), 'test-manager');

        manager.shutDown();
        assert(manager.stopped());
        assert.strictEqual(taskRuns, 3);
        assert.isTrue(manager.isShutDown());

        console.log = logBackup;
    });

    it(`tries to stop tasks`, async function () {
        this.timeout(5000);

        const dir = path.join(__dirname, 'tmp/tasks');
        const manager = new TasksManager(dir);
        await manager.load();

        assert.isArray(manager.paused());
        assert.strictEqual(manager.paused().length, 0);

        manager.pause('by-the-second');
        assert.isArray(manager.paused());
        assert.strictEqual(manager.paused().length, 1);
        assert.isTrue(manager.paused().includes('by-the-second'));
        assert.isFalse(manager.stopped());

        manager.unpause('by-the-second');
        assert.isArray(manager.paused());
        assert.strictEqual(manager.paused().length, 0);
        assert.isFalse(manager.stopped());

        manager.stop();
        assert.isTrue(manager.stopped());
        assert.isArray(manager.paused());
        assert.strictEqual(manager.paused().length, 0);

        manager.unstop();
        assert.isFalse(manager.stopped());
        assert.isArray(manager.paused());
        assert.strictEqual(manager.paused().length, 0);

        manager.shutDown();
        assert.isTrue(manager.stopped());
        assert.isTrue(manager.isShutDown());
    });

    it(`checks generic manager methods`, async function () {
        const dir = path.join(__dirname, 'tmp/tasks');
        const manager = new TasksManager(dir, {
            key: 'test-manager',
        });
        await manager.load();

        assert.sameMembers(manager.directories(), [dir]);
        const items = manager.items();
        assert.isArray(items);
        assert.strictEqual(items.length, 1);
        assert.deepNestedInclude(items[0], {
            name: 'by-the-second',
            path: path.join(__dirname, 'tmp/tasks/by-the-second.task.js'),
        });
        assert.sameMembers(manager.itemNames(), ['by-the-second']);
        assert.isTrue(manager.matchesKey('test-manager'));
        assert.deepNestedInclude(manager.options(), {
            debug: false,
            key: 'test-manager',
            queueTick: 5000,
            runAsQueue: false,
            suffix: 'task',
            verbose: true,
        });

        manager.shutDown();
        assert.isTrue(manager.stopped());
        assert.isTrue(manager.isShutDown());
    });

    it(`checks generic task methods`, async () => {
        assert.strictEqual(byTheSecond.description(), 'A simple task that runs every second');
        assert.strictEqual(byTheSecond.name(), 'By the Second');
        assert.isFalse(byTheSecond.paused());

        const task = new TestClass();
        assert.isNull(task.description());
        assert.strictEqual(task.name(), 'test');
        assert.isFalse(task.paused());
    });
});
