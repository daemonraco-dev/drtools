'use strict';
//
// Dependencies.
const { assert } = require('chai');
const path = require('path');
//
// Testing.
describe(`[015] drtools: mock-routes:`, () => {
    const { MockRoutesManager } = require('..');

    it(`tries to load mock-routes from a directory (using express)`, async function () {
        const express = require('express');

        const app = express();
        const conf = path.join(__dirname, 'tmp/mock-routes/mockup-routes.json');
        const manager = new MockRoutesManager(app, conf);
        await manager.load();

        //// assert.sameMembers(manager.itemNames(), ['users']);
        ////
        //// const routes = manager.routes();
        //// assert.isArray(routes);
        //// assert.strictEqual(routes.length, 1);
        //// assert.deepNestedInclude(routes[0], {
        ////     name: 'users',
        ////     path: path.join(__dirname, 'tmp/routes/users.route.js'),
        ////     routes: [{
        ////         uri: '/users/',
        ////         methods: {
        ////             get: true,
        ////         },
        ////     }],
        //// });
    });

    it(`tries to load mock-routes from a directory (using koa)`, async function () {
        const Koa = require('koa');

        const app = new Koa();
        const conf = path.join(__dirname, 'tmp/mock-routes/mockup-routes.json');
        const manager = new MockRoutesManager(app, conf);
        await manager.load();

        //// assert.sameMembers(manager.itemNames(), ['users']);
        ////
        //// const routes = manager.routes();
        //// assert.isArray(routes);
        //// assert.strictEqual(routes.length, 1);
        //// assert.deepNestedInclude(routes[0], {
        ////     name: 'users',
        ////     path: path.join(__dirname, 'tmp/routes/users.route.koa.js'),
        ////     routes: [{
        ////         uri: '/users/',
        ////         methods: ['HEAD', 'GET'],
        ////     }, {
        ////         uri: '/users([^/]*)',
        ////         methods: []
        ////     }],
        //// });
    });
});
