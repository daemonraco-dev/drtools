'use strict';
//
// Dependencies.
const { assert } = require('chai');
const { ChildProcess } = require('child_process');
const path = require('path');
//
// Testing.
describe(`[011] drtools: Tenedor:`, () => {
    const { Tenedor, TenedorException, TenedorMissingScriptException, TenedorNoChildException } = require('..');

    it(`checks that it can't create an instance`, () => {
        try {
            new Tenedor();
            assert.isTrue(false, 'it should not create an instance.');
        } catch (err) {
            assert.instanceOf(err, TenedorException);
            assert.match(`${err}`, /^.*factory-class.*not possible.*create instances.*$/);
        }
    });

    it(`runs a parallel process successfully`, async () => {
        try {
            const result = await Tenedor.Run(path.join(__dirname, 'tmp/others/tenedor-worker.js'), {
                value: true,
            });
            assert.strictEqual(result, 10);
        } catch (err) {
            assert.isTrue(false, 'it should not fail.');
        }
    });

    it(`runs a parallel process successfully (without extension)`, async () => {
        try {
            const result = await Tenedor.Run(path.join(__dirname, 'tmp/others/tenedor-worker'), {
                value: true,
            });
            assert.strictEqual(result, 10);
        } catch (err) {
            assert.isTrue(false, 'it should not fail.');
        }
    });

    it(`runs a parallel process that fails`, async () => {
        try {
            await Tenedor.Run(path.join(__dirname, 'tmp/others/tenedor-worker'), {
                value: false,
            });
            assert.isTrue(false, 'it should fail.');
        } catch (err) {
            assert.match(`${err}`, /^.*failed.*$/);
        }
    });

    it(`runs a parallel process with a wrong path`, async () => {
        try {
            await Tenedor.Run(path.join(__dirname, 'tmp/others/WRONG-PATH'), {
                value: true,
            });
            assert.isTrue(false, 'it should fail.');
        } catch (err) {
            assert.instanceOf(err, TenedorMissingScriptException);
            assert.match(`${err}`, /^.*WRONG-PATH.*doesn't exist.*$/);
            assert.strictEqual(err.script, path.join(__dirname, 'tmp/others/WRONG-PATH'));
        }
    });

    it(`runs a parallel process that dies instead of responding`, async () => {
        try {
            await Tenedor.Run(path.join(__dirname, 'tmp/others/tenedor-worker'), {
                value: null,
            });
            assert.isTrue(false, 'it should fail.');
        } catch (err) {
            assert.match(`${err}`, /^.*process exited unexpectedly.*$/);
        }
    });

    it(`runs a parallel process that dies too fast`, async () => {
        try {
            await Tenedor.Run(path.join(__dirname, 'tmp/others/tenedor-bad-worker'), {
                value: true,
            });
            assert.isTrue(false, 'it should fail.');
        } catch (err) {
            assert.match(`${err}`, /^.*process exited unexpectedly.*$/);
        }
    });

    it(`runs a parallel process using Spawn()`, async () => {
        try {
            const child = await Tenedor.Spawn(path.join(__dirname, 'tmp/others/tenedor-worker'));
            assert.instanceOf(child.child(), ChildProcess);
            child.kill();
            assert.isNull(child.child());
        } catch (err) {
            assert.isTrue(false, 'it should not fail.');
        }
    });

    it(`runs a parallel process using Spawn() and telling it something`, async () => {
        try {
            const child = await Tenedor.Spawn(path.join(__dirname, 'tmp/others/tenedor-worker'));
            await child.tell({ value: true });
            child.kill();
        } catch (err) {
            assert.isTrue(false, 'it should not fail.');
        }
    });

    it(`runs a parallel process using Spawn() and telling it something after killing it`, async () => {
        try {
            const child = await Tenedor.Spawn(path.join(__dirname, 'tmp/others/tenedor-worker'));
            child.kill();
            await child.tell({ value: true });
            assert.isTrue(false, 'it should fail.');
        } catch (err) {
            assert.instanceOf(err, TenedorNoChildException);
            assert.match(`${err}`, /^.*child process.*not running.*$/i);
        }
    });

    it(`runs a parallel process using Spawn() and asking it something`, async () => {
        try {
            const child = await Tenedor.Spawn(path.join(__dirname, 'tmp/others/tenedor-worker'));
            const result = await child.ask({ value: true });
            assert.strictEqual(result, 10);
            child.kill();
        } catch (err) {
            assert.isTrue(false, 'it should not fail.');
        }
    });

    it(`runs a parallel process using Spawn() and asking it something after killing it`, async () => {
        try {
            const child = await Tenedor.Spawn(path.join(__dirname, 'tmp/others/tenedor-worker'));
            child.kill();
            await child.ask({ value: true });
            assert.isTrue(false, 'it should fail.');
        } catch (err) {
            assert.instanceOf(err, TenedorNoChildException);
            assert.match(`${err}`, /^.*child process.*not running.*$/i);
        }
    });
});
